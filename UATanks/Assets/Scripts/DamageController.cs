﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour
{
    private TankData data; // data from the hit tank
    private float damageTaken; // damage to recive
    public TankData shootingTank; //data from the shooter (MotorManager)

    public ExplosionSound explosionSound;
    public float force = 500;
    public float bulletLife = 3; //bullet should explode
    public Rigidbody bulletRigidbody;


    void Start()
    {
        bulletRigidbody.AddForce(transform.forward * force);
        Destroy(bulletRigidbody.gameObject, bulletLife);
        explosionSound = GameObject.FindObjectOfType(typeof(ExplosionSound)) as ExplosionSound; 
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Player2" || other.tag == "Enemy") // check what hit
        {
            data = other.gameObject.GetComponent<TankData>(); // get tank data from collision
            damageTaken = data.damage; //register damage amount
            data.TakeDamage(damageTaken); //call method to aply damage

            if (shootingTank.tag == "Player")//if player shoots incriment score
            {
                if (data.health <= 0) //if hit tank dies incriment score
                {
                    if (data.bigScore)
                    {
                        GameManager.instance.playerScore += data.scoreValueBig;
                    }
                    if (data.medScore)
                    {
                        GameManager.instance.playerScore += data.scoreValueMed;
                    }
                    if (data.smallScore)
                    {
                        GameManager.instance.playerScore += data.scoreValueSmall;
                    }
                }
            }
            if (shootingTank.tag == "Player2")//if player 2 shoot incriment scores 
            {
                if (data.health <= 0) //if hit tank dies incriment score
                {
                    if (data.bigScore)
                    {
                        GameManager.instance.playerScore += data.scoreValueBig;
                    }
                    if (data.medScore)
                    {
                        GameManager.instance.playerScore += data.scoreValueMed;
                    }
                    if (data.smallScore)
                    {
                        GameManager.instance.playerScore += data.scoreValueSmall;
                    }

                }
            }

            if (data.health <= 0)
            {
                explosionSound.PlayOnDeathSound(); //play explosion sound
                Destroy(other.gameObject);
                if (data.gameObject.CompareTag("Player"))
                {
                    GameManager.instance.playerAlive = false;     //tell game manager player is not alive
                    GameManager.instance.playerlives--;  //reduce player 1 lives by 1
                }
                else if (data.gameObject.CompareTag("Player2"))
                {
                    GameManager.instance.player2Alive = false;   //tell game manager player 2 is not alive
                    GameManager.instance.player2lives--;   //reduce player 2 lives by 1
                }
                else if (data.gameObject.CompareTag("Enemy"))
                {
                    if (data.chaser)
                    {
                        GameManager.instance.chaserAlive = false; // tell manager chaser is dead
                    }
                    if (data.scared)
                    {
                        GameManager.instance.scaredAlive = false; // tell manager chaser is dead
                    }
                    if (data.stationary)
                    {
                        GameManager.instance.stationaryAlive = false; // tell manager chaser is dead
                    }
                    if (data.patroller)
                    {
                        GameManager.instance.patrollerAlive = false; // tell manager chaser is dead
                    }
                }

            }
        }
        else {}//do nothing
        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "wall")
        {
            Destroy(gameObject);    //destroy bullet
        }
    }

}
