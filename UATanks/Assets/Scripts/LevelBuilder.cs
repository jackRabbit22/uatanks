﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour {


    public List<GameObject> roomPrefabs;  //room prefab list to spawn from
    public GameObject gameManager;	//gameManager object

    [Header ("Map Variable")]
    public int cols;	//columns to build
	public int rows;	//rows to build
	public Room [,] grid;	//environment grid
    public bool customSeed;	//use seed when building level
	public int levelSeed;   //level seed
    public float tileWidth = 50;
    public float tileLength = 50;

    [Header("Level of the Day")]
    private System.DateTime today;	//today's date
    private int levelsSaved = 0;  //level generation lock
    private int year;	
	private int day;	
	private int month;	
	private int daySeed; //level of the day seed
	public bool levelOfDay;  //use level of the day seed for level seed
    [Header("Misc")]
    public bool gameReady = false;	//game ready wait
	public static LevelBuilder levelBuild;	//level generation object

	// Use this for initialization
	void Awake ()
    {

		if (levelBuild != null)
        {	//if level is not null
			Destroy (gameObject);
		} else
        {
			levelBuild = this;	
		}

		today = System.DateTime.Now; //set today to today's date
		year = today.Year;	
		day = today.Day;	
		month = today.Month;	
		daySeed = year * 10000 + day + month * 100;	//set daySeed to YYYYDDMM format
	}

	void seedChoice()
    {
		if (levelOfDay)
        {	//if using level of the day seed
			Random.InitState (daySeed); 	//set random initial state to daySeed
		} else if (customSeed)
        {
            Random.InitState (levelSeed);	//set random initial state to levelSeed
		} else
        {
			//do nothing
		}
	}
    
	void Update ()
    {

		if (gameReady == true && levelsSaved == 0)
        {
            seedChoice ();	//run seed choice function
			GenerateGrid (); //generate the level
            gameManager.SetActive(true);  // awake game manager
			levelsSaved++;	 //lock level gen
		}
	}

	public void GenerateGrid ()
    {
		grid = new Room[cols,rows];	 //setup grid array

		for (int i  = 0; i < rows; i++)
        { //for every row
			for (int k = 0; k < cols; k++)
            { //for each column

				Vector3 position = new Vector3 (k * tileWidth, 0, i * tileLength);	//set position based on tile width and length
				GameObject prefabToSpawn = roomPrefabs [Random.Range (0, roomPrefabs.Count)];	//pick room to spawn from list
				GameObject temp = Instantiate (prefabToSpawn, position, Quaternion.identity) as GameObject;		//spawn room
				temp.transform.parent = transform.parent;  //set as parent to make next peice move next to it.


                //Open the doors in the rooms.

                Room tempRoom = temp.GetComponent<Room> (); 
				if (k != 0) {
					tempRoom.doorWest.SetActive (false); //if not first then open west door
				}
				if (k != cols - 1) {
					tempRoom.doorEast.SetActive (false); //if not last then open open east doors
				}
				if (i != 0) {
					tempRoom.doorSouth.SetActive (false); // open south doors if not first in the row
				}
				if (i != rows - 1) {
					tempRoom.doorNorth.SetActive (false); // open north doors if not last.
				}
				grid [k, i] = tempRoom;		//store it in grid

			}
		}
	}
}
