﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    [Header("Options")]
    public bool twoPlayer;
    public float sfxLevel;
    public float musicLevel;

    [Header("Controls")]
    public Slider fxSlider;
    public Slider musicSlider;
    public Toggle twoPlayerToggle;

    public AudioSource menuMusic;
    public AudioSource gameMusic;

    private int twoPlayerStat;
    public bool inGame;

    public static OptionsManager options;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (options != null)
        {  
            Destroy(gameObject);   //destroy that object
        }
        else
        {  
            options = this;   //set os object to this
        }
    }

    void Start()
    {
        twoPlayerStat = PlayerPrefs.GetInt("twoPlayer");  //load twoplayer status from player prefs
        if (twoPlayerStat == 1)
        {   
            twoPlayerToggle.isOn = true;
        }
        else
        {
            twoPlayerToggle.isOn = false;    
        }
        sfxLevel = PlayerPrefs.GetFloat("soundfxOption");       //load sound fx Level from player prefs
        fxSlider.value = sfxLevel;                         //set slider value to sound fx level
        musicLevel = PlayerPrefs.GetFloat("musicOption");       //load musicLevel from player prefs
        musicSlider.value = musicLevel;                         //set slider value to music level
    }

    void Update()
    {
        sfxLevel = fxSlider.value;  
        musicLevel = musicSlider.value; 

        if (twoPlayerStat == 0)
        { 
            twoPlayer = false;    //two player is false
        }
        else if (twoPlayerStat == 1)
        {  
            twoPlayer = true;    //two player is true
        }
        if (menuMusic != null)
        {  
            menuMusic.volume = musicLevel;    //set volume from musicLevel
        }
        if (inGame)
        {   //if in game
            gameMusic = GameObject.FindGameObjectWithTag("GameMusic").GetComponent<AudioSource>();  //find in game music object
            if (gameMusic != null)
            { 
                gameMusic.volume = musicLevel;   //set volume from musicLevel
            }
        }
    }

    public void saveOptions()
    {
        PlayerPrefs.SetInt("twoPlayer", twoPlayerStat);     //set twoPlayerStat to twoPlayer in player prefs
        PlayerPrefs.SetFloat("soundfxOption", sfxLevel);        //set soundLevel to soundOption in player prefs
        PlayerPrefs.SetFloat("musicOption", musicLevel);        //set musicLevel to soundOption in player prefs
        PlayerPrefs.Save();                                 //save player prefs
    }

    public void TwoPlayer()
    {
        if (twoPlayer)
        {
            twoPlayer = false;    //change twoPlayer to false
            twoPlayerStat = 0;   //set stat to 0
        }
        else if (!twoPlayer)
        {
            twoPlayer = true;      //change twoPlayer to true
            twoPlayerStat = 1;    //set stat to 1
        }
    }

}
