﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour
{

    public TankData data; //tank data

    public List<Powerup> powerupsList;  //powerup list


    // Use this for initialization
    void Start()
    {
        data = GetComponent<TankData>();//sets data to tank data
    }


    public void HandlePowerups()
    {
        List<Powerup> powerupsToRemove = new List<Powerup>();
        foreach (Powerup powerUp in powerupsList)
        { //move through list of powerups
            powerUp.lifespan -= Time.deltaTime; //subtract from lifespan
            if (powerUp.lifespan < 0)
            {   //if lifespan is less than 0
                powerUp.RemovePowerup(data);  //apply RemovePowerup function in PowerUp
                powerupsToRemove.Add(powerUp);  //add to remove list
            }
        }
        foreach (Powerup powerUp in powerupsToRemove)
        {  
            powerupsList.Remove(powerUp);  //remove pu from lsit
        }
    }

    // Update is called once per frame
    void Update()
    {
        HandlePowerups();
    }
}
