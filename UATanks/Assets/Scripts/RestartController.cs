﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartController : MonoBehaviour
{

    public GameObject[] toDestroy;

    // Start is called before the first frame update
    void Start()
    {
        if (toDestroy == null)
        {
            toDestroy = GameObject.FindGameObjectsWithTag("GameManager");          //find all game managers
        }
    }

    public void RestartGame()
    {
        for (int i = 0; i < toDestroy.Length; i++)
        {
            Destroy(toDestroy[i].gameObject);           //destroy that game object
        }

        Destroy(GameObject.Find("GameManager"));   //destroy gameManager
        SceneManager.LoadScene(0);   //load start scene
    }
}
