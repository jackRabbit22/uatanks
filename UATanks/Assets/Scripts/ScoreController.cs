﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{

    public Text player1FinalScore;
    public Text player2FinalScore;
    public Text highScore;

    public GameObject player2Score;

    private float player1ScoreFoat;
    private string player1ScoreString;
    private float player2ScoreFloat;
    private string player2ScoreString;
    private float highScoreFloat;
    private string highScoreString;

    void Awake()
    {
        player1ScoreFoat = GameManager.instance.playerScore;   //pull player 1 score from game manager
        player1ScoreString = player1ScoreFoat.ToString();         //convert player1 score float to string
        if (GameManager.instance.twoPlayers == true)
        {   //if twoPlayer mode
            player2ScoreFloat = GameManager.instance.player2Score;       //pull player2 score from game manager
            player2ScoreString = player2ScoreFloat.ToString();     //convert player2 score float to string
        }
        highScoreFloat = GameManager.instance.highscore;          //pull hiscore from game manager
        highScoreString = highScoreFloat.ToString();         //convert hiscore float to string
    }

    // Use this for initialization
    void Start()
    {
        if (GameManager.instance.twoPlayers == false)
        {  
            player2Score.SetActive(false);   //turn off player 2 score
            player1FinalScore.text = player1ScoreString;    //final score text box for player 1 set from player 1 score string
            highScore.text = highScoreString;    //hiscore text box set from hiscore string
        }
        else
        {  
            player1FinalScore.text = player1ScoreString;     //final score text box for player 1 set from player 1 score string
            player2FinalScore.text = player2ScoreString;    //final score text box for player 2 set from player 2 score string
            highScore.text = highScoreString;    //hiscore text box set from hiscore string
        }
    }
}
