﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSpawner : MonoBehaviour {

	public GameObject spawnPowerup;	//prefab for what to spawn
	public float respawnTime;	//how long until spawn
	public bool spawnOnLoad;	//will it spawn on load
	public float timeLeft;  //time till respawn

    private GameObject spawnedObject;	//object that sponed on this place
    private Transform tf;

	void Start ()
    {
		tf = GetComponent<Transform> (); //set tf to Transform component
		if (spawnOnLoad)
        {
			SpawnPowerup (); //spawn powerup
		}
		timeLeft = respawnTime; //set countdown to respawnTime
	}

	void Update () {
		if (spawnedObject == null)
        {
			timeLeft--; 
		}
		if (timeLeft <= 0) { //if countdown = 0 spawn
			SpawnPowerup ();	//spawn powerup
		}	
	}

	void SpawnPowerup () {
		spawnedObject = Instantiate (spawnPowerup, tf.position, tf.rotation) as GameObject;	//spawn powerup
		timeLeft = respawnTime; 	//reset timer
	}
}
