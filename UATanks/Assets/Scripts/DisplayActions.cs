﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AIMoves { n, turn, move };	//obstacle avoidance states
public enum MoveTypes { s, l, p, r };		//patrol states

public class DisplayActions : MonoBehaviour
{
    public AIMoves AIMovesStates;	//obstacle avoidance state
    public MoveTypes moveType;	//patrol loop
    public List<Transform> waypoints;//patrol waypoints
    public int currentWaypoint; //current waypoint 
    public MotorManager AIMotor; //AI motor
    public TankData data;   //tank data
    public bool isPatrollingForward = true;
    public float proximity = 1.0f;
    public float avoidMovementTime = 1.0f;
    public float avoidTimeChange;
    public float avoidDistance = 3.0f;

    public void Start()
    {
        avoidTimeChange = Time.time;  //sets last avoidance state change to current time
    }

    public void Update()
    {
        currentWaypoint = (int)Mathf.Clamp(currentWaypoint, 0, waypoints.Count - 1); //Keep currentWaypoint in bounds
        switch (AIMovesStates)
        {
            case AIMoves.n:
                Vector3 newDirection = waypoints[currentWaypoint].position - data.tf.position;   //waypoint location 
                AIMotor.RotateTowards(newDirection);
                AIMotor.Move(data.moveSpeed);
                float distanceToWaypoint;
                distanceToWaypoint = Vector3.Distance(data.tf.position, waypoints[currentWaypoint].position);   //find distance to waypoint
                if (distanceToWaypoint <= proximity)
                {
                    if (isPatrollingForward)
                    {
                        currentWaypoint++;   //increase current waypoint count
                    }
                    else
                    {    //if not patrolling forward
                        currentWaypoint--;  //decrease current waypoint count
                    }
                    if (isPatrollingForward)
                    {      //if we are patrolling forward
                        if (currentWaypoint >= waypoints.Count)
                        {  //if we are at the last waypoint decide based on loop type
                            switch (moveType)
                            {  //patrolling loop switch statement
                                case MoveTypes.l:
                                    currentWaypoint = 0;  //current waypoint is set back to 0
                                    break;
                                case MoveTypes.r:

                                    break;
                                case MoveTypes.p:   //foward then reverse
                                    isPatrollingForward = false;    //switch isPatrollingForward to false
                                    break;
                                case MoveTypes.s:
                                    //do nothing
                                    break;
                            }
                        }
                    }
                    else
                    {  //when moving backwards
                        if (currentWaypoint >= 0)
                        {
                            switch (moveType)
                            {
                                case MoveTypes.l:
                                    currentWaypoint = waypoints.Count - 1; //reset count
                                    break;
                                case MoveTypes.r:

                                    break;
                                case MoveTypes.p:   //reverse path
                                    isPatrollingForward = true;   //switch isPatrollingForward to true
                                    break;
                                case MoveTypes.s:
                                    //do nothing
                                    break;
                            }
                        }

                    }

                }
                if (!CanMove())
                {
                    ChangeMoveState(AIMoves.turn);
                }
                break;
            case AIMoves.turn:
                AIMotor.Turn(data.turnSpeed);

                if (CanMove())
                {// move when clear
                    ChangeMoveState(AIMoves.move);
                }
                break;
            case AIMoves.move:
                AIMotor.Move(data.moveSpeed);

                if (!CanMove())
                {// turn to avoid
                    ChangeMoveState(AIMoves.turn);
                }
                if (Time.time - avoidTimeChange > avoidMovementTime)
                {// safety measures
                    ChangeMoveState(AIMoves.n);
                }
                break;
        }
    }

    private bool CanMove()
    {
        RaycastHit hitData; //raycast forward
        if (Physics.Raycast(data.tf.position, data.tf.forward, out hitData, avoidDistance))
        {
            if (hitData.collider.tag == "Bullet")
            { //dont count bullets 
                return true;
            }
            return false;
        }
        return true;
    }

    private void ChangeMoveState(AIMoves newState)
    {
        avoidTimeChange = Time.time;                                                      //save time state change
        AIMovesStates = newState;
    }

}