﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupHealth : Pickup
{

    public HealthPowerup powerupData;
    public PickupSound pickupSound;

    private void Start()
    {
        pickupSound = GameObject.FindObjectOfType(typeof(PickupSound)) as PickupSound;
    }

    public override void OnTriggerEnter(Collider other)
    {
        pickupSound.PlayOnPickUp();
        PowerupManager powerManager;
        powerManager = other.gameObject.GetComponent<PowerupManager>(); //get powerup manager from other
        if (powerManager != null)
        {
            powerManager.powerupsList.Add(powerupData); //add powerup to list
            powerupData.ApplyPowerup(powerManager.data); //apply powerup to same TankData as powerup manager
            Destroy(gameObject); //destroys the powerup pickup object 
        }
    }
}
