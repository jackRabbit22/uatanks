﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup
{ //powerup class

    public float lifespan = 2f; // time till powerup runs out

    public virtual void ApplyPowerup(TankData data)
    { //parent ApplyPowerup function
      //Do nothing add sound later
    }

    public virtual void RemovePowerup(TankData data)
    { //parent RemovePowerup function
      //Do nothing add sound later
    }

}

[System.Serializable]
public class HealthPowerup : Powerup // Heals the tank up to full health
{
    public float healthToAdd; //health to add
    public bool maxHealth; //max health clamp
    public override void ApplyPowerup(TankData data)
    {
        base.ApplyPowerup(data);
        data.health += healthToAdd;   //add to health

        if (maxHealth)
        {
            data.health = Mathf.Clamp(data.health, 0, data.maxHealth); //clamp to health
        }
    }
    public override void RemovePowerup(TankData data)
    {
        base.RemovePowerup(data);
        //Do nothing
    }
}

[System.Serializable]
public class SpeedPowerup : Powerup //Speed boost added then removed after lifespan runs out
{
    public float speedBoost = 100;  //speed boost to add

    public override void ApplyPowerup(TankData data)
    {
        base.ApplyPowerup(data);
        data.moveSpeed += speedBoost;   //add speed boost to forwardSpeed
    }
    public override void RemovePowerup(TankData data)
    {
        base.RemovePowerup(data);
        data.moveSpeed -= speedBoost;   //remove speed boost to forwardSpeed
    }
}

[System.Serializable]
public class DamagePowerup : Powerup // timed powerup
{
    public float damageBoost = 50f; //damage boost 
    public float maxDamage; //max damage increase
    public bool isMax; //max damage clamp
    public override void ApplyPowerup(TankData data)
    {
        base.ApplyPowerup(data);
        data.damage += damageBoost;   //add to damage

        if (isMax)
        {
            data.damage = Mathf.Clamp(data.damage, 0, maxDamage); //clamp to damage
        }
    }
    public override void RemovePowerup(TankData data)
    {
        base.RemovePowerup(data);
        data.damage = data.damageOrig; // return damage to original
    }
}


