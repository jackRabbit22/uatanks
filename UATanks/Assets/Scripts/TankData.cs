﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{

    [Header("Atributes")]
    public float health = 100; //health
    public float maxHealth = 300; //max health form powerups
    public float moveSpeed = 3; // meters
    public float turnSpeed = 180; // degrees
    public float timeDelay = 2; //bullet delay before next shot
    public float damage = 25; // damage caused by bullet
    public float damageOrig = 25; // damage of bullet to return after powerup
    [Header("Scoring")]
    public float scoreValueSmall = 10; // score
    public float scoreValueMed = 50; // score med
    public float scoreValueBig = 100; // score big
    public bool bigScore;
    public bool medScore;
    public bool smallScore;

    public bool chaser;
    public bool stationary;
    public bool scared;
    public bool patroller;


    public bool isMakingNoise = false;	//tank noise
    public float fieldOfView = 110;


    public Transform bulletFirePoint;
    public GameObject bulletPrefab;
    public Transform tf;
    public CharacterController controller;
    public DisplayInfo displayInfo;


    private void Awake()
    {
        tf = GetComponent<Transform>();		//sets tf to transform component
        controller = GetComponent<CharacterController>();	//sets controller to character controller component

    }
    void Update()
    {
        if (tag == "Player")
        {
            displayInfo.HealthUpdate(health);
            displayInfo.ScoreUpdate(GameManager.instance.playerScore);
            displayInfo.LivesUpdate(GameManager.instance.playerlives);
        }
        if (tag == "Player2")
        {
            displayInfo.HealthUpdate(health);
            displayInfo.ScoreUpdate(GameManager.instance.player2Score);
            displayInfo.LivesUpdate(GameManager.instance.player2lives);
        }
    }

    public void TakeDamage(float damageDone)
    {  
        health = health - damageDone;    //take damage
    }
}


