﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    [Header("Character Arrays")]
    public List<InputManager> player;   // player array
    public List<GameObject> AIController;   //ai array
    [Header("Prefabs")]
    public GameObject playerPrefab;         //player prefab
    public GameObject player2Prefab;         //player2 prefab
    public GameObject chaserPrefab;         //hunter AI prefab
    public GameObject scaredPrefab;       //defender AI prefab
    public GameObject stationaryPrefab;          //fleer AI prefab
    public GameObject patrollerPrefab;      //patroller AI prefab
    [Header("Object Lists")]
    public List<Transform> AIPatrolPoints;        //AI patrol list
    public List<GameObject> inGamePlayer;	//player object in game
    public List<GameObject> playerSpawn;      //player spawn list
    public List<GameObject> powerupsList;           //powerup spawn list
    public List<GameObject> AISpawn;
    public List<GameObject> roomList; // list for level
    [Header("UI Variables")]
    public float playerScore;
    public float player2Score;
    public float highscore;
    public int playerlives = 3;
    public int player2lives = 3;
    [Header("Game")]
    public bool gameOver = false;    //game over state
    public bool inGame = false;	//in game state
    [Header("Options")]
    public bool twoPlayers;   //twoplayers mode
    public float sound;    //sound volume level
    public float music;   //music volume level
    public GameObject options;	//options game object
    [Header("Status")]
    public bool playerAlive = false;
    public bool player2Alive = false;
    public bool chaserAlive = false;
    public bool stationaryAlive = false;
    public bool scaredAlive = false ;
    public bool patrollerAlive = false;
    [Header("Audio")]
    public GameObject[] gameFx;				//audio sources in game

    private void Awake()
    {
        // Setup the singleton
        if (instance == null) //if game manager is not null
        {
            instance = this;
            DontDestroyOnLoad(gameObject);  //dont destroy this object on scene load
        }
        else
        {
            Destroy(gameObject);
        }
        if (options == null)
        { 
            options = GameObject.FindGameObjectWithTag("OptionsControler");   //find options gameobject
        }
        player.Clear(); // clear array
        inGamePlayer.Clear(); // clear array
    }

    void Start()
    {
        highscore = PlayerPrefs.GetFloat("hiscore");    //load highscore from player prefs
        twoPlayers = OptionsManager.options.twoPlayer;     //pull two player stat from options
        sound = OptionsManager.options.sfxLevel;   //pull sound level stat from options
        music = OptionsManager.options.musicLevel;    //pull music level stat from options
        Destroy(options);	//destroy options 
        inGame = true;  //set inGame state to true

    }

    void Update()
    {
        if (!playerAlive && playerlives > 0 && !gameOver)
        {    //if player is not currently alive 
            SpawnPlayer();       //spawn the player
        }
        if (!player2Alive && twoPlayers && player2lives > 0 && !gameOver)
        {   //if twoplayer and player 2 is not alive and not game over
            SpawnPlayer2();       //spawn player 2
        }
        if (playerAlive && !chaserAlive && !gameOver)
        {                         
            SpawnChaser();    //spawn chaser                                               
        }
        if (playerAlive && !stationaryAlive && !gameOver)
        {                      
            SpawnStationary();      //spawn stationary                                             
        }
        if (playerAlive && !scaredAlive && !gameOver)
        {                         
            SpawnScared();        //spawn scared                                              
        }
        if (playerAlive && !patrollerAlive && !gameOver)
        {                  
            SpawnPatroller();      //spawn patroller                                       
        }

        if (!twoPlayers)
        {                                                       //if not two player mode
            if (playerlives == 0)
            {
                KillAll(); 
                gameOver = true;  //game over state is true
                SceneManager.LoadScene("End");                                  //load end scene
                playerlives = -1;                                                     //set lives to anything but 0
            }
        }
        else if (twoPlayers)
        {                                               //if two player mode
            if (playerlives == 0 && player2lives == 0)
            {                                   //and both lives is 0
                KillAll();                                                      //run kill function
                gameOver = true;                                                //game over state is true
                SceneManager.LoadScene("End");                                  //load end scene
                playerlives = -1;                                                     //set lives to anything but 0
                player2lives = -1;                                                   //set player 2 lives to anything but 0
            }
        }

        ScoreUpdate();
        if (inGame)
        {  
            gameFx = GameObject.FindGameObjectsWithTag("TankSFX");  //find all tank audio
            for (int i = 0; i < gameFx.Length; i++)
            {  
                AudioSource temp = gameFx[i].GetComponent<AudioSource>();      //create temp audio source object 
                if (temp != null)
                {  
                    temp.volume = sound; //assign volume level to sound
                }
            }
        }
    }


    public void KillAll()
    {   //kill function
        if (AIController.Count > 0)
        {    //until all AI tanks are destroyed
            foreach (GameObject toDestroy in AIController)
            {   //each object in array
                Destroy(toDestroy); //destroy game object
            }
        }
    }


    public void ScoreUpdate()
    {      //score keeping function
        if (!twoPlayers)
        { 
            if (highscore < playerScore)
            {        //if score is higher than hiscore
                highscore = playerScore;        //set hiscore to score
            }
        }
        else if (twoPlayers)
        {       //if two player
            if (playerScore > player2Score)
            {             //if score player1 is higher than player2
                if (highscore < playerScore)
                {            //if score player1 is higher than hiscore
                    highscore = playerScore;        //set hiscore to score player1
                }
            }
            else
            {   
                if (highscore < player2Score)
                {        //if score player2 is higher than hiscore
                    highscore = player2Score;     //set hiscore to score player2
                }
            }
        }
        if (gameOver)
        {      //if game over state is true
            PlayerPrefs.SetFloat("hiscore", highscore);    //set hiscore to hiscore in player prefs
            PlayerPrefs.Save();        //save player prefs
        }
    }

    public void SpawnPlayer()
    {                                                                                                     
        int spawnPoints = Random.Range(0, playerSpawn.Count);        //pick a random spawn from the playerSpawn list
        Instantiate(playerPrefab, playerSpawn[spawnPoints].transform.position, playerSpawn[spawnPoints].transform.rotation);   //spawn player at chosen spawn point
        playerAlive = true;                                                                                                 
    }

    public void SpawnPlayer2()
    {
        int spawnPoints = Random.Range(0, playerSpawn.Count);         //pick a random spawn from the playerSpawn list
        Instantiate(player2Prefab, playerSpawn[spawnPoints].transform.position, playerSpawn[spawnPoints].transform.rotation);  //spawn player at chosen spawn point
        player2Alive = true;    
    }

    public void SpawnChaser()
    {                                                                                                    
        int AIPoints = Random.Range(0, AISpawn.Count);    //pick a random spawn from AISpawn list
        Instantiate(chaserPrefab, AISpawn[AIPoints].transform.position, AISpawn[AIPoints].transform.rotation);   //spawn hunter at chosen point
        chaserAlive = true;   
    }

    public void SpawnStationary()
    {                                                                                              
        int AIPoints = Random.Range(4, AISpawn.Count-1  );  //pick a random spawn from AISpawn 
        Instantiate(stationaryPrefab, AISpawn[AIPoints].transform.position, AISpawn[AIPoints].transform.rotation);    //spawn defender at chosen point
        stationaryAlive = true;                                                                                                     
    }

    public void SpawnScared()
    {                                                                                                     
        int AIPoints = Random.Range(3, AISpawn.Count-2);          //pick a random spawn from AISpawn list
        Instantiate(scaredPrefab, AISpawn[AIPoints].transform.position, AISpawn[AIPoints].transform.rotation);     //spawn scared at chosen point
        scaredAlive = true;                                                                                                          
    }

    public void SpawnPatroller()
    {
        int AIPoints = Random.Range(0, AISpawn.Count);                //pick a random spawn from AISpawn list
        Instantiate(patrollerPrefab, AISpawn[AIPoints].transform.position, AISpawn[AIPoints].transform.rotation);   //spawn patroller at chosen point
        patrollerAlive = true;

    }
}


