﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera[] cameras;

    void Update()
    {
        if (LevelBuilder.levelBuild.gameReady)
        {
            cameras = FindObjectsOfType<Camera>();
            if (!GameManager.instance.twoPlayers)
            {
                cameras[0].rect = new Rect(0, 0, 1, 1);
            }
            else if (GameManager.instance.twoPlayers)
            {
                cameras[0].rect = new Rect(0, 0, 1, 0.499f);
                cameras[1].rect = new Rect(0, 0.502f, 1, 0.499f);
            }

        }
    }
}
