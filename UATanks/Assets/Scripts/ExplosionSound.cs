﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSound : MonoBehaviour
{
    public AudioSource tankExplosion;

    public void PlayOnDeathSound()
    {
        tankExplosion.Play(); // play sound
    }
}
