﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour {

    public enum InputScheme { WASD, arrowKeys };
    public InputScheme input = InputScheme.arrowKeys;

    public MotorManager motor;
    public TankData data; // tank data


    private void Awake()
    {
        if (tag == "Player")
        {
            GameManager.instance.player.Insert(0, this);  //player is set in game manager
            GameManager.instance.inGamePlayer.Insert(0, this.gameObject);   //player is set in game manager
        }
        else if (tag == "Player2")
        {
            GameManager.instance.player.Insert(1, this);  //player is set in game manager
            GameManager.instance.inGamePlayer.Insert(1, this.gameObject);     //player is set in game manager
        }
    }

    void OnDestroy()
    {
        if (tag == "Player")
        {
            GameManager.instance.player.RemoveAt(0);  //player is set in game manager
            GameManager.instance.inGamePlayer.RemoveAt(0);     //player is set in game manager
        }
        else if(tag == "Player2")
        {
            GameManager.instance.player.RemoveAt(1);  //player is set in game manager
            GameManager.instance.inGamePlayer.RemoveAt(1);     //player is set in game manager
        }

    }

    private void Start()
    {
        data = gameObject.GetComponent<TankData>(); //sets data to tank data
        motor = gameObject.GetComponent<MotorManager>(); // sets motor to Motor manager
    }

    void FixedUpdate () {

        switch (input)
        {
            case InputScheme.arrowKeys: // Arrow key input
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    motor.Move(data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    motor.Move(-data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    motor.Turn(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    motor.Turn(-data.turnSpeed);
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    motor.Shoot();
                }
                if (Input.GetKey(KeyCode.Escape))
                {
                    SceneManager.LoadScene(0);
                }
                if (Input.GetKey(KeyCode.P))
                {
                    SceneManager.LoadScene(2);
                }
                break;
            case InputScheme.WASD: //WASD input
                if (Input.GetKey(KeyCode.W))
                {
                    motor.Move(data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    motor.Move(-data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    motor.Turn(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    motor.Turn(-data.turnSpeed);
                }
                if (Input.GetKeyDown(KeyCode.G))
                {
                    motor.Shoot();
                }
                if (Input.GetKey(KeyCode.Escape))
                {
                    SceneManager.LoadScene(0);
                }
                if (Input.GetKey(KeyCode.P))
                {
                    SceneManager.LoadScene(2);
                }
                break;
        }
    }    
}
