﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorManager : MonoBehaviour {

    [SerializeField]
    private CharacterController characterController;

    public TankData data;
    public Transform tf;
    private float lastEventTime; // timer

    public AudioSource tankFireSound;


    private void Awake()
    {
        data = gameObject.GetComponent<TankData>(); //set data to tank data
        tf = gameObject.GetComponent<Transform>(); // set tf to transform
        characterController = gameObject.GetComponent<CharacterController>();

        // save time 00:00
        lastEventTime = Time.time;
    }

    public void Move(float moveSpeed)
    {
        //Move character in specified direction.
        characterController.SimpleMove(tf.forward * moveSpeed * Time.deltaTime);
    }
    //overide
    public void Move(Vector3 speedAndDirection)
    {                                               //controller says move
        characterController.SimpleMove(speedAndDirection);                                                //move in direction recieved by controller
    }
    public void Turn(float turnSpeed)
    {
        // Turn character at specified speed.
        tf.Rotate((Vector3.up * turnSpeed * Time.deltaTime), Space.Self);
    }
    //overide
    public void Rotate(Vector3 rotationSpeed)
    {                                                   //controller says rotate
        data.tf.Rotate(rotationSpeed);                                                                //rotate in direction recieved by controller
    }
    public void RotateTowards(Vector3 newDirection)// rotate twords something
    {
        Quaternion goalRotation;  
        goalRotation = Quaternion.LookRotation(newDirection);
        data.tf.rotation = Quaternion.RotateTowards(data.tf.rotation, goalRotation, data.turnSpeed);      //rotate towards our target rotation
    }

    public void Shoot()
    {
     
        //check time pased since started or last time it was pressed.
        if (Time.time >= lastEventTime + data.timeDelay)
        {
            tankFireSound.Play();
            Instantiate(data.bulletPrefab, data.bulletFirePoint.position, data.bulletFirePoint.rotation);

            DamageController temp = GetComponent<DamageController>(); //save the data of shooting tank
            temp.shootingTank = data; //send it over to damage controler.   

            // save last time it was called
            lastEventTime = Time.time;
        }
    }
}
