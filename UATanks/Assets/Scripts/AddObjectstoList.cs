﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddObjectstoList : MonoBehaviour
{
    public bool playerSpawn;
    public bool patrolPoint;
    public bool powerup;
    public bool AISpawn;
    public bool roomSpawn;

    // Start is called before the first frame update
    void Start()
    {
        if (playerSpawn)
        {                                           //if isPlayerSpawn is true
            GameManager.instance.playerSpawn.Add(this.gameObject);
        }
        else if (patrolPoint)
        {
            GameManager.instance.AIPatrolPoints.Add(this.gameObject.transform); //add it to Patrol points list in game manager
        }
        else if (powerup)
        {
            GameManager.instance.powerupsList.Add(this.gameObject);     //add it to powerupList list in game manager

        }
        else if (AISpawn)
        {                                          
            GameManager.instance.AISpawn.Add(this.gameObject); // add it to ai spown list
        }
        else if (roomSpawn)
        {
            GameManager.instance.roomList.Add(this.gameObject);
        }
}
}
