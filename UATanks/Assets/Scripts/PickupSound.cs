﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSound : MonoBehaviour
{
    public AudioSource pickupSound;

    public void PlayOnPickUp()
    {
        pickupSound.Play(); //play sound
    }
}
