﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayInfo : MonoBehaviour {

    public Text livesTextBox;  //lives text box
    public Text scoreTextBox;    //score text box
    public Text healthTextBox;   //health text box

    private float healthNumber;   //health float
    private string healthString;   //health string
    private float scoreNumber;   //score float
    private string scoreString;  //score string
    private float livesNumber;   //lives float
    private string livesString;  //lives string

    public void HealthUpdate(float h)
    {
        healthNumber = h;     //assign health float to input argument
        healthString = healthNumber.ToString(); //convert float to string
        healthTextBox.text = healthString;          //assign new string to text box
    }

    public void ScoreUpdate(float s)
    {
        scoreNumber = s;       //assign score float to input argument
        scoreString = scoreNumber.ToString();   //convert float to string
        scoreTextBox.text = scoreString;            //assign new string to text box
    }

    public void LivesUpdate(float l)
    {
        livesNumber = l;        //assign lives float to input argument
        livesString = livesNumber.ToString();   //convert float to string
        livesTextBox.text = livesString;            //assign new string to text box
    }
}
