﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuControler : MonoBehaviour
{
    [Header("Mainmenu")]
    public GameObject startButton;
    public GameObject optionsButton;
    public GameObject endButton;
    public GameObject backButton;
    public GameObject musicSlider;
    public GameObject soundFxSlider;
    public GameObject twoPlayerToggle;

    [Header("LevelBuildScreen")]
    public GameObject levelSelectCamera;
    public GameObject canvas;
    public GameObject eventSys;
    public GameObject randomButton;
    public GameObject customButton;
    public GameObject levelDayButton;
    public GameObject seedInput;
    public GameObject confirmButton;
    public GameObject mainMenuButton;
    public InputField textbox;
    public GameObject displayTank;

    [Header("Options")]
    public OptionsManager optionSetting;

    public void StartGame()
    {
        SceneManager.LoadScene(1);  //load main scene
    }

    public void EndGame()
    {
        Application.Quit(); //exit application
    }

    public void OptionsButton()
    {
        startButton.SetActive(false);        //turn off button
        optionsButton.SetActive(false);     //turn off button
        endButton.SetActive(false);         //turn off button
        backButton.SetActive(true);           //turn on button
        musicSlider.SetActive(true);            //turn on slider
        soundFxSlider.SetActive(true);            //turn on slider
        twoPlayerToggle.SetActive(true);        //turn on toggle
    }

    public void BackButton()
    {
        startButton.SetActive(true);            //turn on button
        optionsButton.SetActive(true);          //turn on button
        endButton.SetActive(true);              //turn on button
        backButton.SetActive(false);          //turn off button
        musicSlider.SetActive(false);           //turn off slider
        soundFxSlider.SetActive(false);           //turn off slider
        twoPlayerToggle.SetActive(false);       //turn off toggle
        optionSetting.saveOptions();                   //run saveOptions function
    }

    public void MainMenuButton()
    {
        SceneManager.LoadScene(0); // return to main menue
    }

    public void RandomSeed()
    {
        LevelBuilder.levelBuild.customSeed = false;        //use seed set to false
        LevelBuilder.levelBuild.levelOfDay = false;     //use level of day seed set to false
        Destroy(levelSelectCamera);                       //destroy seed cam
        canvas.SetActive(false);                //turn off canvas
        eventSys.SetActive(false);              //turn off event system
        displayTank.SetActive(false);           //turn off display tank
        LevelBuilder.levelBuild.gameReady = true;       //tell level builder game is ready

    }

    public void CustomSeed()
    {
        LevelBuilder.levelBuild.customSeed = true;         //use seed set to true
        LevelBuilder.levelBuild.levelOfDay = false;     //use level of day seed set to false
        randomButton.SetActive(false);               //turn off button
        customButton.SetActive(false);               //turn off button
        levelDayButton.SetActive(false);            //turn off button
        seedInput.SetActive(true);              //turn on text input
        confirmButton.SetActive(true);          //turn on button
        backButton.SetActive(true);         //turn on button
        LevelBuilder.levelBuild.gameReady = false;      //tell level builder game is still not ready
    }

    public void LeveloftheDaySeed()
    {
        LevelBuilder.levelBuild.customSeed = false;        //use seed set to false
        LevelBuilder.levelBuild.levelOfDay = true;      //use level of day seed set to true
        Destroy(levelSelectCamera);                       //destroy seed cam
        canvas.SetActive(false);                //turn off canvas
        eventSys.SetActive(false);              //turn off event system
        displayTank.SetActive(false);           //turn off display tank
        LevelBuilder.levelBuild.gameReady = true;       //tell level builder game is ready
    }

    public void ComfirmSeed()
    {
        LevelBuilder.levelBuild.levelSeed = int.Parse(textbox.text);    //pull data from text input and assign it to level seed in level builder
        Destroy(levelSelectCamera);                                       //destroy seed cam
        canvas.SetActive(false);                                //turn off canvas
        eventSys.SetActive(false);                              //turn off event system
        displayTank.SetActive(false);                           //turn off display tank
        LevelBuilder.levelBuild.gameReady = true;                       //tell level builder game is ready
    }

}
